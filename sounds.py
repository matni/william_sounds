# Generate sounds for William experiments

import os
import numpy as np
import sounddevice as sd
import time  # Function time-sleep()
import soundfile as sf  # Write to .wav
import pickle  # Storing data

import mn  # User made functions for sounds


# Set and display sound device, output device marked <
# sd.default.device = "Analog (3+4) (RME Babyface Pro), MME" # "ASIO HDSPe FX" 
print('Sound device --------------------------------------------------------- ')
print(sd.query_devices())
print('-----------------------------------------------------------------------')


# Preliminaries
fs = 48000  # sampling frequncy
sgain = 0.5 # Relative amplitude (max = 1)
dur = 1.0  # duration in seconds
f0 = 400 # Fundamental frequency


def generate_sound(maxn = 23, dbmin = -30, mistune = 0.02): 
    ''' Fuction to generate harmonic complex (signal), with random variation in 
    number of harmonics, and their frequency and level.
 
    Arguments:
    maxn -- Maximum number of harmonics (including f0), selected randomly between
            1 and maxn
    dbmin -- Level of harmonics: random value in the interval [dbmin, 0]
    mistune -- Mistunig of harmonics (f1, f2, ...) by multiplying by a randomly 
               selected factor in the interval [1-mistune, 1+mistune].
    Output:
    s -- Generated signal
    f -- Harmonic frequencies
    db -- Harmonic levels 
    '''

   
    ## Set parameters of signal ------------------------------------------------    
    # Random number of harmonics (including f0)
    n = np.random.randint(3, maxn, size = 1, dtype=int)  # Minimum 3 harmomics 
    f = f0 * (np.arange(n) + 1)  # Harmonic frequencies, f0, f1, ..., f(n-1)

    # Random levels of harmonics
    gain_db = np.random.uniform(low = dbmin, high = 0, size = n)
    gain_db = np.round(gain_db) # Round to integer
    gain = 10**(gain_db/20.0)

    # Random mistuning of harmonics f1, f2, ..., fn
    low = 1 - mistune  # lowest mistuning factor
    high = 1 + mistune # highest mistuning factor
    mistune = np.random.uniform(low = low, high = high, size = n-1)
    mistune = np.append([1.0], mistune)
    fm = np.round(f * mistune)  # Round to integer
    ## -------------------------------------------------------------------------

    ## Generate signal ---------------------------------------------------------
    s = np.zeros(int(fs*dur))  # Empty vector to be filled with sinusoids
    
    for j in range(len(gain_db)):
        tone = mn.create_sinusoid(freq = fm[j], phase = 0, fs = 48000, dur = dur)
        s = s + gain[j]*tone
    
    s = mn.set_gain(s, gaindb = -20.0)  # Normalize rms
    s = mn.fade(s, fs * 0.01)
    s  = np.transpose(np.array([s, s]))  # Make stereo
    ## -------------------------------------------------------------------------
    print("gain_db)", gain_db)
    return s, fm, gain_db


def save_sounds(ss, subfolder = "generated_wav"):  
    ''' Function to save many (ss) randomly generated sounds as *wav.   
        They are saved in a subfolder with default name "generated_wav".
        Function still nder construction: Proeprties of sounds should be stored
        and saved somehow! '''
    os.mkdir(subfolder)
    sound_infp = list()  # Empty list, to be filled with dictionaries
    for j in range(ss):
        snumber = j + 1
        print(snumber)
        s, f, db = generate_sound(maxn = 20, dbmin = -30, mistune = 0.02)

        # Write recording to .wav
        snumber = str(int(snumber + 1000))  
        snumber = snumber[1:4]  # Make number 001, 002, ... 
        filename = subfolder + "/" + 'sound_' + snumber + '.wav'
        sf.write(filename, s, int(fs), subtype = "FLOAT")

# Run this line to save ss generated sounds in a subfolder
# save_sounds(ss = 20, subfolder = "test")  


## Test: Listen to a single sound ----------------------------------------------
# Generate sound
s, f, db = generate_sound(maxn = 23, dbmin = -30, mistune = 0.02)

# Play sound
print('\nSignal is playing ---------------------------------------------------')
print("f: ", f)
print("dB: ", db)
time.sleep(0.4)
sd.play(s, fs)
time.sleep(dur + 0.2)
print('\nSignal stopped playing ------------------------------------------\n\n\n')
## -----------------------------------------------------------------------------