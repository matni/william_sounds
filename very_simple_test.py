# Simple scirpt for testing sounds

import numpy as np
import sounddevice as sd
import time  # Function time-sleep()
import mn  # User made functions for sounds


# Set and display sound device, output device marked <
# Line below chooses sound card, needed later on when we are in the sound lab
# sd.default.device = "Analog (3+4) (RME Babyface Pro), MME" # "ASIO HDSPe FX" 
print('Sound device --------------------------------------------------------- ')
print(sd.query_devices())
print('-----------------------------------------------------------------------')


# Set these parameters
fs = 48000  # sampling frequncy
sgain = 0.5 # Relative amplitude (max = 1)
dur = 1.0  # duration of signal in seconds

# Generate signal = sinusoid
s = mn.create_sinusoid(freq = 1000, phase = 0, fs = fs, dur = dur)
s = sgain * mn.fade(s, fs * 0.01)  # Fade-in out, 10 ms
s  = np.transpose(np.array([s, s]))  # Make stereo

# Play tone
print('\nTone is playing ------------------------------------------\n\n\n')
time.sleep(0.4)
sd.play(s, fs)
time.sleep(dur + 0.2)
print('\ntone stopped playing ------------------------------------------\n\n\n')


