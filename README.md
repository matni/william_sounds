**Python scripts for generating sounds for training experiment memory**

Hi William, Here we may put python scripts for generating sounds. For the moment there are three files:

+ mn.py -- My library with useful functions for dealig with sounds
+ very_simple_test.py -- Run this to make sure you can generate and play a sound using library mn and sounddevice
+ sounds.py -- Script for generating experimental sounds. Still under construction, 
but you may test it out now. Uncomment line with # save_sounds(...) and run the script, 
it will generate as many files you like (< 1000) and store them in a folder that you 
may give a name using argument subfolder.

Easiest way to work with the files is just to clone the full repository and run files from the cloned directory. You will need a few python libraries, you may already have them installed (if not, try pip)

+ numpy 
+ sounddevice
+ time



Best Mats

PS: I made the repository public, so you and anyone can clone it (why not?)
